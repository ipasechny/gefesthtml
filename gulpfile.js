
/**
 * yarn add typescript
 * yarn add gulp gulp-uglify gulp-imagemin gulp-concat gulp-rename
 * yarn add gulp-favicons gulp-sourcemaps cssnano rucksack-css gulp-postcss
 * yarn add postcss-import postcss-mixins postcss-nested postcss-nested-ancestors
 * yarn add postcss-calc postcss-simple-vars gulp-browser-js-include gulp-browser-js-include browser-sync
**/

var gulp         = require('gulp'),
	
	include      = require('gulp-browser-js-include'),
	
	concat       = require('gulp-concat'),
	rename       = require('gulp-rename'),
	uglify       = require('gulp-uglify'),
	imagemin     = require('gulp-imagemin'),
	favicons     = require('gulp-favicons'),
	mqcombine    = require('gulp-combine-mq'),
	sourcemaps   = require('gulp-sourcemaps'),
	
	postcss      = require('gulp-postcss'),
	cssnano      = require('cssnano'),
	lostgrid     = require('lost'),
	rucksack     = require('rucksack-css'),
	
	css_vars     = require('postcss-simple-vars'),
	css_calc     = require('postcss-calc'),
	css_terms    = require('postcss-conditionals'),
	css_import   = require('postcss-import'),
	css_mixins   = require('postcss-mixins'),
	css_nested   = require('postcss-nested'),
	css_parent   = require('postcss-nested-ancestors'),
	
	browserSync = require('browser-sync').create(),
	reload = browserSync.reload,
	src = {
		js: 'assets/js/',
		css: 'assets/css/',
		img: 'assets/img/',
		icons: 'assets/icons/',
		fonts: 'assets/fonts/'
	};
	
/*[js][:]*/
gulp.task('js', function() {
	return (
		gulp.src('source/js/*.js')
			.pipe(include())
			.pipe(gulp.dest(src.js)),
		gulp.src(['source/js/fallbacks/**/*.*'])
			.pipe(gulp.dest('assets/js/fallbacks/'))
	);
});
gulp.task('js:build', function() {
	return (
		gulp.src([
				'source/js/jquery.min.js',
				'source/js/plugin.min.js',
				'source/js/script.js'
			])
			.pipe(sourcemaps.init())
			.pipe(uglify())
			.pipe(concat('script.min.js'))
			.pipe(sourcemaps.write('.'))
			.pipe(gulp.dest(src.js))
	);
});
/*[js][;]*/

/*[css][:]*/
gulp.task('css', function () {
	var processors = [
		css_import,
		css_mixins,

		css_vars,
		css_calc({
			mediaQueries: true
		}),
		css_terms,
		css_parent,
		css_nested,

		lostgrid(),

		rucksack({
			alias: false,
			clearFix: false,
			responsiveType: false,
			shorthandPosition: false
		}),

		cssnano({
			//core: false,
			zindex: false,
			autoprefixer: {
				add: true,
				browsers: [
					'> 1%',
					'ie >= 9',
					'last 3 versions'
				]
			},
			discardComments: {
				removeAll: true
			}
		})
	];

	return (
		gulp.src('source/css/style.css')
			.pipe(sourcemaps.init())
			.pipe(postcss(processors))
			.pipe(
				mqcombine({
					beautify: false
				})
			)
			.pipe(rename('style.min.css'))
			.pipe(sourcemaps.write('.'))
			.pipe(gulp.dest(src.css))
	);
});
/*[css][;]*/

/*[image][:]*/
gulp.task('img', function() {
	return (
		gulp.src('source/img/**/*')
			.pipe(gulp.dest(src.img))
	);
});
gulp.task('img:build', function() {
	return (
		gulp.src('source/img/**/*')
			.pipe(
				imagemin([
					imagemin.svgo({
						plugins: [{
							cleanupIDs: false,
							removeUselessDefs: false
						}]
					}),
					imagemin.optipng(),
					imagemin.gifsicle(),
					imagemin.jpegtran()
				])
			)
			.pipe(gulp.dest(src.img))
	);
});
/*[image][;]*/

/*[media][:]*/
gulp.task('media', function() {
	return (
		gulp.src(['source/media/**/*.*'])
			.pipe(gulp.dest('assets/media/'))
	);
});
/*[media][;]*/

/*[favicon][:]*/
gulp.task('favicon', function() {
	return (
		gulp.src('source/img/favicon.svg')
			.pipe(
				favicons({
					background: 'transparent',
					icons: {
						coast: false,
						yandex: false,
						firefox: false,
						windows: false,
						android: false,
						favicons: true,
						appleIcon: true,
						appleStartup: false,
					}
				})
			)
			.pipe(gulp.dest(src.icons))
	);
});
/*[favicon][;]*/

/*[server][:]*/
gulp.task('watch', function() {
	gulp.watch('source/js/**/*.*', ['js']);
	gulp.watch('source/css/**/*.*', ['css']);
	gulp.watch('source/img/**/*.*', ['img']);
	gulp.watch('source/media/**/*.*', ['media']);
});
gulp.task('server', ['js', 'css', 'img', 'media'], function() {
	browserSync.init({
		notify: false,
		server: {
			baseDir: './'
		}
	});
	
	gulp.watch('*.html').on('change', reload);
	gulp.watch('assets/**/*').on('change', reload);
});
/*[server][;]*/

gulp.task('build', [
	'js:build',
	'img:build'
]);
gulp.task('default', [
	'js',
	'img',
	'css',
	'media',
	'watch',
	'server'
]);


