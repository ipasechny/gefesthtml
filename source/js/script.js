
$(function() {
	'use strict';

	var scroll_size = scrollbarSize();

	/*[================  FN  ================][:]*/
	//= include parts/functions.js

	/*[================ MENU ================][:]*/
	//= include parts/menu/menu_mobile.js
	//= include parts/menu/menu_rating.js

	/*[================ SHOW ================][:]*/
	//= include parts/show/show_main.js
	//= include parts/show/show_tease.js

	/*[================ BASE ================][:]*/
	//= include parts/base/esc.js
	//= include parts/base/news.js
	//= include parts/base/share.js
	//= include parts/base/print.js
	//= include parts/base/popup.js
	//= include parts/base/search.js
	//= include parts/base/subscribe.js
	//= include parts/base/hero_item.js
	//= include parts/base/scroll_up.js
	//= include parts/base/popup_video.js

	/*[================ FORM ================][:]*/
	//= include parts/form/fn.js
	//= include parts/form/config.js
	//= include parts/form/form_list.js
	//= include parts/form/form_mail.js
	//= include parts/form/form_news.js
	//= include parts/form/form_goods.js


	/*[========== ========== ========== ========== ==========]*/

	if (!device.desktop()) {
		$('.news_item_video, .show_main_video').remove();

		window.addEventListener('load', function() {
			setTimeout(scrollTo, 0, 0, 1);
		}, false);
	}

});

$(window).on('load', function() {
	setTimeout(function() {
		$('.preloader').removeClass('_show');
	}, 600);

	$('.show_main_video').each(function() {
		var $this = $(this);
		if ($this.attr('preload') === 'none') {
			$this.prop('preload', 'auto');
		}
	});
});


