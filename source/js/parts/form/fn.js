
	/*[fn][:]*/
	function fnFormClear(form) {
		var $form = $(form),
			$fields = $form.find('.input, .textarea');
		$fields.removeClass('_error _valid').val('');
	}
	function fnFormAnswer(type, data) {
		if (type === 'done') {
			$popup_txt.html(data.message);
			$popup_msg.open();
		} else {
			$popup_txt.html(data.error);
			$popup_msg.open();
		}
	}
	function fnFormSubmit(form, beforeCallback, sendCallback, errorCallback) {
		var $form = $(form),
			data = $form.serialize(),
			url = $form.attr('action') || '#',
			method = $form.attr('method') || 'post';

		if (typeof(beforeCallback) === 'function') {
			beforeCallback();
		}

		$.ajax({
			url: url,
			data: data,
			type: method,
			dataType: 'json',
			error: function(data) {
				if (typeof(errorCallback) === 'function') {
					errorCallback(data);
				}
			},
			success: function(data) {
				if (data.success) {
					fnFormClear(form);
					if (typeof(sendCallback) === 'function') {
						sendCallback(data);
					}
				} else {
					if (typeof(errorCallback) === 'function') {
						errorCallback(data);
					}
				}
			}
		});
	}
	function fnFormValidate(form, beforeCallback, sendCallback, errorCallback) {
		$(form).validate({
			ignore: ':not([required])',
			rules: {
				/*hiddenRecaptcha: {
					required: function() {
						if (grecaptcha.getResponse() === '') {
							return true;
						} else {
							return false;
						}
					}
				}*/
			},
			errorPlacement: function() {
				return false;
			},
			submitHandler: function() {
				fnFormSubmit(form, beforeCallback, sendCallback, errorCallback);
			}
		});
	}
	/*[fn][:]*/