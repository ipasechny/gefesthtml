
	/*[config][:]*/
	$.validator.setDefaults({
		errorClass: '_error',
		validClass: '_valid'
	});

	$.validator.addMethod('text', function(value, element) {
		return this.optional(element) || /^[a-zа-я\s]+$/i.test(value);
	}, 'Letters only please');
	$.validator.addMethod('textn', function(value, element) {
		return this.optional(element) || /^[a-zа-я0-9\s]+$/i.test(value);
	}, 'Letters and numbers only please');
	$.validator.addMethod('textnp', function(value, element) {
		return this.optional(element) || /^[a-zа-я0-9\-.,()'"\s]+$/i.test(value);
	}, 'Letters, numbers and punctuation only please');
	$.validator.addMethod('extension', function(value, element, param) {
		param = typeof param === 'string' ? param.replace(/,/g, '|') : 'png|jpe?g|gif';
		return this.optional(element) || value.match(new RegExp('\\.(' + param + ')$', 'i'));
	}, $.validator.format('Please enter a value with a valid extension.'));
	$.validator.addMethod('phone', function(value, element) {
		return this.optional(element) || /^\+\d{3}\s\(\d{2}\)\s\d{3}-\d{2}-\d{2}$/i.test(value);
	}, 'Please specify a valid phone number');

	$.validator.addClassRules('rule_text', {text: true});
	$.validator.addClassRules('rule_email', {email: true});
	$.validator.addClassRules('rule_phone', {phone: true});
	$.validator.addClassRules('rule_textn', {textn: true});
	$.validator.addClassRules('rule_textnp', {textnp: true});
	$.validator.addClassRules('rule_file', {extension: 'pdf|txt|doc?x|xls?x|jpe?g|gif|png|rar|zip'});
	/*[config][;]*/