
	/*[menu_mobile][:]*/
	$('.menu_mobile_button').on('click', function() {
		var $this = $(this),
			$menu = $('.menu_mobile');

		if ($this.hasClass('_active')) {
			$this.removeClass('_active');
			$menu.removeClass('_show');
			fnShowScroll();
		} else {
			$('html, body').animate({scrollTop: 0}, 400);
			$this.addClass('_active');
			$menu.addClass('_show');
			fnHideScroll();
		}
	});
	/*[menu_mobile][;]*/