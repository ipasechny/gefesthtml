
	/*[show_tease][:]*/
	var space_20 = (100 / (1280 / 20)) + '%';

	$('.js_show_tease').swiper({
		autoplay: 6000,
		slidesPerView: 3,
		longSwipesRatio: 1,
		spaceBetween: space_20,
		breakpoints: {
			1280: {
				spaceBetween: 20,
				slidesPerView: 2
			},
			768: {
				slidesPerView: 1
			}
		}
	});
	/*[show_tease][;]*/