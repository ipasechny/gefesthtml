
	/*[show_main][:]*/
	(function() {
		if ($('.js_show_main').length) {
			var fnShowMainVideoStop = function() {
				var video, check,
					$videos = $('.show_main_video');
				
				for (video of $videos) {
					check = video.readyState > 2 &&
								video.currentTime > 0 &&
									!(video.ended && video.paused);

					if (check) {
						video.pause();
						video.currentTime = 0;
						$(video).removeClass('_play');
					}
				}
			};
			var fnShowMainVideoPlay = function(swiper) {
				fnShowMainVideoStop();

				var index = swiper.activeIndex,
					slide = swiper.slides[index],
					$video = $(slide).find('.show_main_video');
				if ($video.length) {
					$video.addClass('_play').get(0).play();
				}
			};

			var show_main = new Swiper('.js_show_main', {
				loop: true,
				speed: 400,
				effect: 'fade',
				longSwipesRatio: 1,
				pagination: '.show_main_dots',
				onTransitionEnd: function(swiper) {
					var $video = $('.show_main_video._play'),
						$slide = $video.parent('.show_main_item');

					if (!$slide.hasClass('_active')) {
						fnShowMainVideoPlay(swiper);
					}
				}
			});

			for (var video of $('.show_main_video')) {
				video.addEventListener('ended', function() {
					$('.show_main_video').removeClass('_play');
					video.currentTime = 0;
					show_main.slideNext();
				}, false);
			}

			$('.show_main_down').on('click', function() {
				var y = $('.show_main').outerHeight();
				$('html, body').animate({scrollTop: y}, 400);
				return false;
			});
		}
	})();
	/*[show_main][;]*/