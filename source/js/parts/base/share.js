
	/*[share][:]*/
	var Share = {
		vk: function(type) {
			var o = this,
				url = 'https://vk.com/share.php?url=';

			url += o.url();
			o.popup(url);
		},
		ok: function(type) {
			var o = this,
				url = 'https://connect.ok.ru/dk?cmd=WidgetSharePreview&st.cmd=WidgetSharePreview&st._aid=ExternalShareWidget_SharePreview&st.shareUrl=';

			url += o.url();
			o.popup(url);
		},
		gp: function(type) {
			var o = this,
				url = 'https://plus.google.com/share?url=';

			url += o.url();
			o.popup(url);
		},
		fb: function(type) {
			var o = this,
				url = 'https://www.facebook.com/sharer/sharer.php?u=';

			url += o.url();
			o.popup(url);
		},
		tw: function(type) {
			var o = this,
				url = 'https://twitter.com/intent/tweet?url=';

			url += o.url();
			o.popup(url);
		},
		url: function() {
			var href = location.href,
				url = encodeURIComponent(href);
			return url;
		},
		popup: function(url) {
			window.open(url, 'share', 'toolbar=0,status=0,width=626,height=436');
		}
	};

	$('.menu_share_link').on('click', function() {
		var $this = $(this),
			type = $this.data('type'),
			$menu = $this.parents('.menu_share'),
			role = $menu.data('role');
		
		if (role === 'share') {
			if (type === 'mail') {
				$popup_mail.open();
			} else {
				switch(type) {
					case 'vk': Share.vk(); break;
					case 'fb': Share.fb(); break;
					case 'tw': Share.tw(); break;
					case 'gp': Share.gp(); break;
					case 'ok': Share.ok(); break;
				}
			}
		} return false;
	});
	/*[share][;]*/