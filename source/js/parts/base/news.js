
	/*[news][:]*/
	var newsItemVideoTimeout;

	$('.news_filter').on('click', function() {
		var y = $('.search').offset().top;
		$('html, body').animate({scrollTop: y}, 400);
	});

	$('.news_loader').on('click', function() {
		var $this = $(this),
			url = $this.attr('href');

		$.ajax({
			url: url,
			type: 'post',
			dataType: 'json',
			success: function(data) {
				if (data.success) {
					$('.news_base').append(data.html);
					
					if (data.next) {
						$this.removeClass('_hide')
							 .attr('href', data.next);
					} else {
						$this.addClass('_hide');
					}
				}
			}
		}); return false;
	});

	$(document).on('mouseenter', '.news_item_video', function() {
		this.play();
		clearTimeout(newsItemVideoTimeout);
	});

	$(document).on('mouseleave', '.news_item_video', function() {
		var video = this,
			check = video.readyState > 2 &&
					video.currentTime > 0 &&
					!(video.ended && video.paused);

		clearTimeout(newsItemVideoTimeout);
		newsItemVideoTimeout = setTimeout(function() {
			if (check) {
				video.pause();
				video.currentTime = 0;
			}
		}, 600);
	});
	/*[news][;]*/