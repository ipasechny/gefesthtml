
	/*[subscribe][:]*/
	if ($('.subscribe').length) {
		$(window).on('scroll resize', function() {
			var $item = $('.subscribe'),
				win_height = $(window).outerHeight(),
				itm_height = $item.outerHeight(),
				s_top = $(window).scrollTop(),
				s_bot = s_top + win_height,
				i_top = $item.offset().top,
				i_bot = i_top + itm_height,
				path_full,
				path_pass,
				progress,
				begin;

			if (s_bot >= i_top) {
				begin = i_top - win_height;
				
				path_full = i_bot - begin;
				path_pass = i_bot - s_top;

				progress = 100 - Math.ceil(100 / (path_full / path_pass));

				progress = (progress >= 0) ? progress : 0;
				progress = (progress <= 100) ? progress : 100;

				$item.css({backgroundPositionY: progress + '%'});
			}
		}).trigger('scroll');
	}
	/*[subscribe][;]*/