
	/*[search][:]*/
	$('.search_more').on('click', function() {
		var $this = $(this),
			$form = $('.form_options');
		if ($this.hasClass('_active')) {
			$form.slideUp(400, function() {
				$form.removeClass('_show');
			});
			$this.removeClass('_active');
		} else {
			$form.slideDown(400, function() {
				$form.addClass('_show');
			});
			$this.addClass('_active');
		}
	});

	if ($('.inner').length) {
		var findNews = function(form, type) {
			var $next = $('.news_loader'),
				y = $('.news').offset().top;

			fnFormSubmit(form, false, function(data) {
				if (data.success) {
					if (type === 'reset') {
						$('.news_info').removeClass('_show');
						$('.news_subtitle').removeClass('_hide');
					} else {
						$('.news_info').addClass('_show');
						$('.news_subtitle').addClass('_hide');
					}

					$('.news_base').html(data.html);
					$('.news_found_num').html(data.num);
					$('html, body').animate({scrollTop: y}, 400);

					if (data.next) {
						$next.removeClass('_hide')
							 .attr('href', data.next);
					} else {
						$next.addClass('_hide');
					}
				}
			});
		}

		$('#form_search').on('submit', function() {
			findNews('#form_search');
			return false;
		});
		$('#form_options').on('submit', function() {
			findNews('#form_options');
			return false;
		});
		$('.form_options_reset').on('click', function() {
			event.preventDefault();
			$('#form_options').get(0).reset();
			$('.form_options_item').removeAttr('checked');
			history.replaceState(null, null, location.pathname);
			findNews('#form_options', 'reset');
		});
	}
	/*[search][;]*/