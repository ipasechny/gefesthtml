
	/*[popup_video][:]*/
	var popup_video = document.getElementById('popup_video');

	function fnPopupVideoHide() {
		if ($('.popup_video').hasClass('_show')) {
			$('.popup_video').removeClass('_show');
			popup_video.currentTime = 0;
			popup_video.pause();

			setTimeout(function() {
				$('.popup_video').addClass('_hide');
				fnShowScroll();
			}, 600);
		}
	}

	$('.js_popup_video_close').on('click', function() {
		fnPopupVideoHide();
	});

	$(document).on('click', '.show_main_play', function() {
		var $this = $(this),
			img = $this.data('img'),
			src = $this.data('src');
		$('.popup_video').addClass('_show').removeClass('_hide');
		$('.popup_video_item').attr({src: src, poster: img});
		popup_video.play();
		fnHideScroll();
	});
	/*[popup_video][;]*/