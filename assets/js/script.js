
$(function() {
	'use strict';

	var scroll_size = scrollbarSize();

	/*[================  FN  ================][:]*/
	
	function fnHideScroll() {
		$('html').addClass('no_scroll').css('padding-right', scroll_size);
	}
	function fnShowScroll() {
		$('html').removeClass('no_scroll').css('padding-right', '');
	}

	/*[================ MENU ================][:]*/
	
	/*[menu_mobile][:]*/
	$('.menu_mobile_button').on('click', function() {
		var $this = $(this),
			$menu = $('.menu_mobile');

		if ($this.hasClass('_active')) {
			$this.removeClass('_active');
			$menu.removeClass('_show');
			fnShowScroll();
		} else {
			$('html, body').animate({scrollTop: 0}, 400);
			$this.addClass('_active');
			$menu.addClass('_show');
			fnHideScroll();
		}
	});
	/*[menu_mobile][;]*/
	
	/*[menu_rating][:]*/
	/*var $menu_rating = $('.menu_rating'),
		is_menu_rating_disable = $menu_rating.hasClass('_disable');

	$('.menu_rating_link').hover(function() {
		if (!is_menu_rating_disable) {
			$(this).addClass('_hover').prevAll().addClass('_hover');
			$('.menu_rating_link').removeClass('_active');
		}
	}, function() {
		if (!is_menu_rating_disable) {
			var index = $('.menu_rating').data('value') - 1;
			$('.menu_rating_link').removeClass('_hover')
					.eq(index).addClass('_active')
							.prevAll().addClass('_active');
		}
	});

	$('.menu_rating_link').on('click', function() {
		if (!is_menu_rating_disable) {
			var value = $(this).index() + 1;
			$menu_rating.data('value', value);
		} else {
			$popup_login.open();
		} return false;
	});*/
	/*[menu_rating][;]*/

	/*[================ SHOW ================][:]*/
	
	/*[show_main][:]*/
	(function() {
		if ($('.js_show_main').length) {
			var fnShowMainVideoStop = function() {
				var video, check,
					$videos = $('.show_main_video');
				
				for (video of $videos) {
					check = video.readyState > 2 &&
								video.currentTime > 0 &&
									!(video.ended && video.paused);

					if (check) {
						video.pause();
						video.currentTime = 0;
						$(video).removeClass('_play');
					}
				}
			};
			var fnShowMainVideoPlay = function(swiper) {
				fnShowMainVideoStop();

				var index = swiper.activeIndex,
					slide = swiper.slides[index],
					$video = $(slide).find('.show_main_video');
				if ($video.length) {
					$video.addClass('_play').get(0).play();
				}
			};

			var show_main = new Swiper('.js_show_main', {
				loop: true,
				speed: 400,
				effect: 'fade',
				longSwipesRatio: 1,
				pagination: '.show_main_dots',
				onTransitionEnd: function(swiper) {
					var $video = $('.show_main_video._play'),
						$slide = $video.parent('.show_main_item');

					if (!$slide.hasClass('_active')) {
						fnShowMainVideoPlay(swiper);
					}
				}
			});

			for (var video of $('.show_main_video')) {
				video.addEventListener('ended', function() {
					$('.show_main_video').removeClass('_play');
					video.currentTime = 0;
					show_main.slideNext();
				}, false);
			}

			$('.show_main_down').on('click', function() {
				var y = $('.show_main').outerHeight();
				$('html, body').animate({scrollTop: y}, 400);
				return false;
			});
		}
	})();
	/*[show_main][;]*/
	
	/*[show_tease][:]*/
	var space_20 = (100 / (1280 / 20)) + '%';

	$('.js_show_tease').swiper({
		autoplay: 6000,
		slidesPerView: 3,
		longSwipesRatio: 1,
		spaceBetween: space_20,
		breakpoints: {
			1280: {
				spaceBetween: 20,
				slidesPerView: 2
			},
			768: {
				slidesPerView: 1
			}
		}
	});
	/*[show_tease][;]*/

	/*[================ BASE ================][:]*/
	
	/*[esc][:]*/
	$(document).on('keyup', function(e) {
		if (e.keyCode === 27) {
			fnPopupVideoHide();
		}
	});
	/*[esc][;]*/
	
	/*[news][:]*/
	var newsItemVideoTimeout;

	$('.news_filter').on('click', function() {
		var y = $('.search').offset().top;
		$('html, body').animate({scrollTop: y}, 400);
	});

	$('.news_loader').on('click', function() {
		var $this = $(this),
			url = $this.attr('href');

		$.ajax({
			url: url,
			type: 'post',
			dataType: 'json',
			success: function(data) {
				if (data.success) {
					$('.news_base').append(data.html);
					
					if (data.next) {
						$this.removeClass('_hide')
							 .attr('href', data.next);
					} else {
						$this.addClass('_hide');
					}
				}
			}
		}); return false;
	});

	$(document).on('mouseenter', '.news_item_video', function() {
		this.play();
		clearTimeout(newsItemVideoTimeout);
	});

	$(document).on('mouseleave', '.news_item_video', function() {
		var video = this,
			check = video.readyState > 2 &&
					video.currentTime > 0 &&
					!(video.ended && video.paused);

		clearTimeout(newsItemVideoTimeout);
		newsItemVideoTimeout = setTimeout(function() {
			if (check) {
				video.pause();
				video.currentTime = 0;
			}
		}, 600);
	});
	/*[news][;]*/
	
	/*[share][:]*/
	var Share = {
		vk: function(type) {
			var o = this,
				url = 'https://vk.com/share.php?url=';

			url += o.url();
			o.popup(url);
		},
		ok: function(type) {
			var o = this,
				url = 'https://connect.ok.ru/dk?cmd=WidgetSharePreview&st.cmd=WidgetSharePreview&st._aid=ExternalShareWidget_SharePreview&st.shareUrl=';

			url += o.url();
			o.popup(url);
		},
		gp: function(type) {
			var o = this,
				url = 'https://plus.google.com/share?url=';

			url += o.url();
			o.popup(url);
		},
		fb: function(type) {
			var o = this,
				url = 'https://www.facebook.com/sharer/sharer.php?u=';

			url += o.url();
			o.popup(url);
		},
		tw: function(type) {
			var o = this,
				url = 'https://twitter.com/intent/tweet?url=';

			url += o.url();
			o.popup(url);
		},
		url: function() {
			var href = location.href,
				url = encodeURIComponent(href);
			return url;
		},
		popup: function(url) {
			window.open(url, 'share', 'toolbar=0,status=0,width=626,height=436');
		}
	};

	$('.menu_share_link').on('click', function() {
		var $this = $(this),
			type = $this.data('type'),
			$menu = $this.parents('.menu_share'),
			role = $menu.data('role');
		
		if (role === 'share') {
			if (type === 'mail') {
				$popup_mail.open();
			} else {
				switch(type) {
					case 'vk': Share.vk(); break;
					case 'fb': Share.fb(); break;
					case 'tw': Share.tw(); break;
					case 'gp': Share.gp(); break;
					case 'ok': Share.ok(); break;
				}
			}
		} return false;
	});
	/*[share][;]*/
	
	/*[print][:]*/
	$('.js_print').on('click', function() {
		window.print();
	});
	/*[print][;]*/
	
	/*[popup][:]*/
	var $popup_txt = $('.js_popup_txt'),
		$popup_msg = $('.js_popup_msg').remodal(),
		$popup_mail = $('.js_popup_mail').remodal(),
		$popup_list = $('.js_popup_list').remodal(),
		$popup_login = $('.js_popup_login').remodal();
	/*[popup][;]*/
	
	/*[search][:]*/
	$('.search_more').on('click', function() {
		var $this = $(this),
			$form = $('.form_options');
		if ($this.hasClass('_active')) {
			$form.slideUp(400, function() {
				$form.removeClass('_show');
			});
			$this.removeClass('_active');
		} else {
			$form.slideDown(400, function() {
				$form.addClass('_show');
			});
			$this.addClass('_active');
		}
	});

	if ($('.inner').length) {
		var findNews = function(form, type) {
			var $next = $('.news_loader'),
				y = $('.news').offset().top;

			fnFormSubmit(form, false, function(data) {
				if (data.success) {
					if (type === 'reset') {
						$('.news_info').removeClass('_show');
						$('.news_subtitle').removeClass('_hide');
					} else {
						$('.news_info').addClass('_show');
						$('.news_subtitle').addClass('_hide');
					}

					$('.news_base').html(data.html);
					$('.news_found_num').html(data.num);
					$('html, body').animate({scrollTop: y}, 400);

					if (data.next) {
						$next.removeClass('_hide')
							 .attr('href', data.next);
					} else {
						$next.addClass('_hide');
					}
				}
			});
		}

		$('#form_search').on('submit', function() {
			findNews('#form_search');
			return false;
		});
		$('#form_options').on('submit', function() {
			findNews('#form_options');
			return false;
		});
		$('.form_options_reset').on('click', function() {
			event.preventDefault();
			$('#form_options').get(0).reset();
			$('.form_options_item').removeAttr('checked');
			history.replaceState(null, null, location.pathname);
			findNews('#form_options', 'reset');
		});
	}
	/*[search][;]*/
	
	/*[subscribe][:]*/
	if ($('.subscribe').length) {
		$(window).on('scroll resize', function() {
			var $item = $('.subscribe'),
				win_height = $(window).outerHeight(),
				itm_height = $item.outerHeight(),
				s_top = $(window).scrollTop(),
				s_bot = s_top + win_height,
				i_top = $item.offset().top,
				i_bot = i_top + itm_height,
				path_full,
				path_pass,
				progress,
				begin;

			if (s_bot >= i_top) {
				begin = i_top - win_height;
				
				path_full = i_bot - begin;
				path_pass = i_bot - s_top;

				progress = 100 - Math.ceil(100 / (path_full / path_pass));

				progress = (progress >= 0) ? progress : 0;
				progress = (progress <= 100) ? progress : 100;

				$item.css({backgroundPositionY: progress + '%'});
			}
		}).trigger('scroll');
	}
	/*[subscribe][;]*/
	
	/*[hero_item][:]*/
	var hero_item_video = document.getElementById('hero_item_video');

	$('.hero_item_play').on('click', function() {
		hero_item_video.setAttribute('controls', true);
		$(this).addClass('_hide');
		hero_item_video.play();
	});
	/*[hero_item][;]*/
	
	/*[scroll_up][:]*/
	$('.js_scroll_up').on('click', function() {
		$('html, body').animate({scrollTop: 0}, 400);
		return false;
	});
	/*[scroll_up][;]*/
	
	/*[popup_video][:]*/
	var popup_video = document.getElementById('popup_video');

	function fnPopupVideoHide() {
		if ($('.popup_video').hasClass('_show')) {
			$('.popup_video').removeClass('_show');
			popup_video.currentTime = 0;
			popup_video.pause();

			setTimeout(function() {
				$('.popup_video').addClass('_hide');
				fnShowScroll();
			}, 600);
		}
	}

	$('.js_popup_video_close').on('click', function() {
		fnPopupVideoHide();
	});

	$(document).on('click', '.show_main_play', function() {
		var $this = $(this),
			img = $this.data('img'),
			src = $this.data('src');
		$('.popup_video').addClass('_show').removeClass('_hide');
		$('.popup_video_item').attr({src: src, poster: img});
		popup_video.play();
		fnHideScroll();
	});
	/*[popup_video][;]*/

	/*[================ FORM ================][:]*/
	
	/*[fn][:]*/
	function fnFormClear(form) {
		var $form = $(form),
			$fields = $form.find('.input, .textarea');
		$fields.removeClass('_error _valid').val('');
	}
	function fnFormAnswer(type, data) {
		if (type === 'done') {
			$popup_txt.html(data.message);
			$popup_msg.open();
		} else {
			$popup_txt.html(data.error);
			$popup_msg.open();
		}
	}
	function fnFormSubmit(form, beforeCallback, sendCallback, errorCallback) {
		var $form = $(form),
			data = $form.serialize(),
			url = $form.attr('action') || '#',
			method = $form.attr('method') || 'post';

		if (typeof(beforeCallback) === 'function') {
			beforeCallback();
		}

		$.ajax({
			url: url,
			data: data,
			type: method,
			dataType: 'json',
			error: function(data) {
				if (typeof(errorCallback) === 'function') {
					errorCallback(data);
				}
			},
			success: function(data) {
				if (data.success) {
					fnFormClear(form);
					if (typeof(sendCallback) === 'function') {
						sendCallback(data);
					}
				} else {
					if (typeof(errorCallback) === 'function') {
						errorCallback(data);
					}
				}
			}
		});
	}
	function fnFormValidate(form, beforeCallback, sendCallback, errorCallback) {
		$(form).validate({
			ignore: ':not([required])',
			rules: {
				/*hiddenRecaptcha: {
					required: function() {
						if (grecaptcha.getResponse() === '') {
							return true;
						} else {
							return false;
						}
					}
				}*/
			},
			errorPlacement: function() {
				return false;
			},
			submitHandler: function() {
				fnFormSubmit(form, beforeCallback, sendCallback, errorCallback);
			}
		});
	}
	/*[fn][:]*/
	
	/*[config][:]*/
	$.validator.setDefaults({
		errorClass: '_error',
		validClass: '_valid'
	});

	$.validator.addMethod('text', function(value, element) {
		return this.optional(element) || /^[a-zа-я\s]+$/i.test(value);
	}, 'Letters only please');
	$.validator.addMethod('textn', function(value, element) {
		return this.optional(element) || /^[a-zа-я0-9\s]+$/i.test(value);
	}, 'Letters and numbers only please');
	$.validator.addMethod('textnp', function(value, element) {
		return this.optional(element) || /^[a-zа-я0-9\-.,()'"\s]+$/i.test(value);
	}, 'Letters, numbers and punctuation only please');
	$.validator.addMethod('extension', function(value, element, param) {
		param = typeof param === 'string' ? param.replace(/,/g, '|') : 'png|jpe?g|gif';
		return this.optional(element) || value.match(new RegExp('\\.(' + param + ')$', 'i'));
	}, $.validator.format('Please enter a value with a valid extension.'));
	$.validator.addMethod('phone', function(value, element) {
		return this.optional(element) || /^\+\d{3}\s\(\d{2}\)\s\d{3}-\d{2}-\d{2}$/i.test(value);
	}, 'Please specify a valid phone number');

	$.validator.addClassRules('rule_text', {text: true});
	$.validator.addClassRules('rule_email', {email: true});
	$.validator.addClassRules('rule_phone', {phone: true});
	$.validator.addClassRules('rule_textn', {textn: true});
	$.validator.addClassRules('rule_textnp', {textnp: true});
	$.validator.addClassRules('rule_file', {extension: 'pdf|txt|doc?x|xls?x|jpe?g|gif|png|rar|zip'});
	/*[config][;]*/
	
	/*[form_list][:]*/
	fnFormValidate('#form_list', false, function(data) {
		fnFormAnswer('done', data);
	}, function(data) {
		fnFormAnswer('fail', data);
	});
	/*[form_list][;]*/
	
	/*[form_mail][:]*/
	fnFormValidate('#form_mail', false, function(data) {
		fnFormAnswer('done', data);
	}, function(data) {
		fnFormAnswer('fail', data);
	});
	/*[form_mail][;]*/
	
	/*[form_news][:]*/
	fnFormValidate('#form_news', false, function(data) {
		fnFormAnswer('done', data);
	}, function(data) {
		fnFormAnswer('fail', data);
	});
	/*[form_news][;]*/
	
	/*[form_goods][:]*/
	$('#form_goods').on('submit', function(e) {
		var data = $(this).serialize();
		$('.js_form_list_data').val(data);
		$popup_list.open();
		return false;
	});
	/*[form_goods][;]*/


	/*[========== ========== ========== ========== ==========]*/

	if (!device.desktop()) {
		$('.news_item_video, .show_main_video').remove();

		window.addEventListener('load', function() {
			setTimeout(scrollTo, 0, 0, 1);
		}, false);
	}

});

$(window).on('load', function() {
	setTimeout(function() {
		$('.preloader').removeClass('_show');
	}, 600);

	$('.show_main_video').each(function() {
		var $this = $(this);
		if ($this.attr('preload') === 'none') {
			$this.prop('preload', 'auto');
		}
	});
});


